package ticketingsystem;

public enum TicketStatus {
	NEW, IN_PROGRESS, REJECTED, RESOLVED
}
