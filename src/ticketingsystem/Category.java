package ticketingsystem;

public enum Category {
	WATER, NETWORK, CHAIRS, TABLE, SPACE, COFFEE_TEA, CUPS
}
