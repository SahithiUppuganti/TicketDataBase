package ticketingsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeSet;

import com.mysql.jdbc.Statement;

public class TicketSource {
	private TicketSource() {
	}

	public static final TicketSource instance = new TicketSource();
	TreeSet<Ticket> treeSet = new TreeSet<Ticket>();

	public static TicketSource getInstance() {
		return instance;
	}

	Ticket addTicket(String title, Category category, String description, String raisedBy) {
		Ticket ticket = new Ticket();
		ticket.setUniqueId(Ticket.count++);
		ticket.setTitle(title);
		ticket.setDescription(description);
		ticket.setTicketStatus(TicketStatus.NEW);
		ticket.setCategory(category);
		ticket.setTicketRaisedBy(raisedBy);
		ticket.setTicketCreationTime(Calendar.getInstance().getTime());
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Timestamp date = new Timestamp(Calendar.getInstance().getTime().getTime());
			String query = " insert into ticket(uniqueId,title,description,ticketStatus,category,tikcetRaisedBy,creationtime)"
					+ " values (?, ?, ?, ?, ?, ?,?)";
			PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setInt(1, ticket.getUniqueId());
			preparedStmt.setString(2, ticket.getTitle());
			preparedStmt.setString(3, ticket.getDescription());
			preparedStmt.setString(4, ticket.getTicketStatus().name());
			preparedStmt.setString(5, ticket.getCategory().name());
			preparedStmt.setString(6, ticket.getTicketRaisedBy());
			preparedStmt.setTimestamp(7, date);
			preparedStmt.executeUpdate();
			connection.close();
		} catch (Exception e) {
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
		return ticket;
	}

	Ticket updateTicket(int uniqueId, String title, Category category, String description, String raisedBy,
			TicketStatus ticketStatus) {
		Ticket ticket = new Ticket();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Timestamp date = new Timestamp(Calendar.getInstance().getTime().getTime());
			String query = "update ticket set title=?,description=?,ticketStatus=?,category=?,tikcetRaisedBy=?,creationtime=? where uniqueId=?";
			PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, title);
			preparedStmt.setString(2, description);
			preparedStmt.setString(3, ticketStatus.name());
			preparedStmt.setString(4, category.name());
			preparedStmt.setString(5, raisedBy);
			preparedStmt.setTimestamp(6, date);
			preparedStmt.setInt(7, uniqueId);
			preparedStmt.executeUpdate();
			connection.close();
		} catch (Exception e) {
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		}
		return ticket;
	}

	public TreeSet<Ticket> getTicketByTitle(String title) {
		TreeSet<Ticket> ticketList = new TreeSet<Ticket>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Statement statement = (Statement) connection.createStatement();
			String strSelect = "select * from ticket where title='" + title + "'";
			ResultSet resultSet = statement.executeQuery(strSelect);
			while (resultSet.next()) {
				Ticket ticket = new Ticket();
				ticket.setUniqueId(resultSet.getInt("uniqueId"));
				ticket.setCategory(Category.valueOf(resultSet.getString("category")));
				ticket.setDescription(resultSet.getString("description"));
				ticket.setTicketStatus(TicketStatus.valueOf(resultSet.getString("ticketStatus")));
				ticket.setTicketCreationTime(resultSet.getTimestamp("creationtime"));
				ticket.setTicketRaisedBy(resultSet.getString("tikcetRaisedBy"));
				ticket.setTitle(resultSet.getString("title"));
				ticketList.add(ticket);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketList;
	}

	public TreeSet<Ticket> getTicketByCategory(Category category) {
		TreeSet<Ticket> ticketList = new TreeSet<Ticket>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Statement statement = (Statement) connection.createStatement();
			String strSelect = "select * from ticket where category='" + category + "'";
			ResultSet resultSet = statement.executeQuery(strSelect);
			while (resultSet.next()) {

				Ticket ticket = new Ticket();
				ticket.setUniqueId(resultSet.getInt("uniqueId"));
				ticket.setTitle(resultSet.getString("title"));
				ticket.setCategory(Category.valueOf(resultSet.getString("category")));
				ticket.setDescription(resultSet.getString("description"));
				ticket.setTicketStatus(TicketStatus.valueOf(resultSet.getString("ticketStatus")));
				ticket.setTicketRaisedBy(resultSet.getString("tikcetRaisedBy"));
				ticket.setTicketCreationTime(resultSet.getTimestamp("creationtime"));
				ticketList.add(ticket);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketList;
	}

	public TreeSet<Ticket> getTicketByStatus(TicketStatus ticketStatus) {
		TreeSet<Ticket> ticketList = new TreeSet<Ticket>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Statement statement = (Statement) connection.createStatement();
			String strSelect = "select * from ticket where ticketStatus='" + ticketStatus + "'";
			ResultSet resultSet = statement.executeQuery(strSelect);
			while (resultSet.next()) {
				Ticket ticket = new Ticket();
				ticket.setUniqueId(resultSet.getInt("uniqueId"));
				ticket.setCategory(Category.valueOf(resultSet.getString("category")));
				ticket.setDescription(resultSet.getString("description"));
				ticket.setTicketStatus(TicketStatus.valueOf(resultSet.getString("ticketStatus")));
				ticket.setTicketCreationTime(resultSet.getTimestamp("creationtime"));
				ticket.setTicketRaisedBy(resultSet.getString("tikcetRaisedBy"));
				ticket.setTitle(resultSet.getString("title"));
				ticketList.add(ticket);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketList;
	}

	public TreeSet<Ticket> getMostRecentTicket() {
		TreeSet<Ticket> ticketList = new TreeSet<Ticket>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Statement statement = (Statement) connection.createStatement();
			String strSelect = "select * from ticket order by creationtime DESC";
			ResultSet resultSet = statement.executeQuery(strSelect);
			int count=0;
			while (resultSet.next()) {
				Ticket ticket = new Ticket();
				ticket.setUniqueId(resultSet.getInt("uniqueId"));
				ticket.setCategory(Category.valueOf(resultSet.getString("category")));
				ticket.setDescription(resultSet.getString("description"));
				ticket.setTicketStatus(TicketStatus.valueOf(resultSet.getString("ticketStatus")));
				ticket.setTicketCreationTime(resultSet.getTimestamp("creationtime"));
				ticket.setTicketRaisedBy(resultSet.getString("tikcetRaisedBy"));
				ticket.setTitle(resultSet.getString("title"));
				ticketList.add(ticket);
				count++;
				if(count==4)
				{
					break;
				}
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketList;
	}

	public TreeSet<Ticket> getByUniqueId(int uniqueId) {
		TreeSet<Ticket> ticketList = new TreeSet<Ticket>();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ticket", "root", "rgukt123");
			Statement statement = (Statement) connection.createStatement();
			String strSelect = "select * from ticket where uniqueId='" + uniqueId + "'";
			ResultSet resultSet = statement.executeQuery(strSelect);
			while (resultSet.next()) {
				Ticket ticket = new Ticket();
				ticket.setUniqueId(resultSet.getInt("uniqueId"));
				ticket.setCategory(Category.valueOf(resultSet.getString("category")));
				ticket.setDescription(resultSet.getString("description"));
				ticket.setTicketStatus(TicketStatus.valueOf(resultSet.getString("ticketStatus")));
				ticket.setTicketCreationTime(resultSet.getTimestamp("creationtime"));
				ticket.setTicketRaisedBy(resultSet.getString("tikcetRaisedBy"));
				ticket.setTitle(resultSet.getString("title"));
				ticketList.add(ticket);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ticketList;
	}
}
